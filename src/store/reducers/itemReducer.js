import { GET_ITEM, GET_ITEMS } from '../actions/types';

export default function itemReducer(state = {}, action) {
    switch (action.type) {
      case GET_ITEM:
        return {...state, ...action.payload};
      case GET_ITEMS:
        return {...state, ...action.payload};
      default:
        return state;
    }
}