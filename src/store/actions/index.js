import {GET_ITEM, GET_ITEMS} from './types';
import axios from 'axios';
import { serialize } from '../../commons/utls';
import { getSession, setSession } from '../../service/SessionService';

const url = '/api/items';

const createHeaders = () => ({
    AUTHOR: JSON.stringify(getSession().author)
})

const createPayloadItem = (data, type) => {
    setSession({author: data.author});
    return {
        type,
        payload: {...data.item}
    }
}

export const getItem = (id) => {
    return (dispatch) => {
        return axios({
            method:'get',
            url: `${url}/${id}`,
            headers: createHeaders()
        })
            .then( r => dispatch(createPayloadItem(r.data, GET_ITEM)) )
            .catch( err => {throw(err)} );
    }
}

const createPayloadItems = (data, type) => {
    setSession({author: data.author});
    return {
        type,
        payload: {...data}
    }
}

export const getItems = (body) => {
    return (dispatch) => {
        return axios({
            method:'get',
            url: `${url}?${serialize(body)}`,
            headers: createHeaders()
        })
        .then( r => dispatch(createPayloadItems(r.data, GET_ITEMS)) )
            .catch( err => {throw(err)} );
    }
}

