import React, {Component} from 'react';
import { connect } from 'react-redux';
import { getItems } from '../../store/actions';

import ResultItem from './components/ResultItem';
import Breadcrumb from '../../components/breadcrumb/Breadcrumb';
import "./Results.sass";


class Results extends Component {
    constructor() {
        super();
        this.state = {
            query: null,
            results: null
        }
        this.updated = false;
    }

    componentDidMount(){
        this.updateQueryParams();
    }

    componentDidUpdate(){
        this.updateQueryParams()
    }

    updateQueryParams = () => {
        if(!this.updated) {
            const {location} = this.props;
            const params = new URLSearchParams(location.search);
            const query = {q: params.get('search')};
            if(!query.q) {
                return;
            }
            this.props.getItems(query)
            this.updated = true;
            return;
        }
        this.updated = false;        
    }

    render() {
        const {results} = this.props;
        return results.items ? <div className="wrapper">
            <div className="mc-results">
                <div className="mc-results__breadcrumb">
                    <Breadcrumb categoriesList={results.categories}/>
                </div>
                <div className="mc-results__container">
                    {results.items.map((value, index) => <ResultItem key={index} {...value} />)}
                </div>
            </div>
        </div> : ''
    }
}

const mapStateToProps = state => {
    return {
        results: state.items
    };
  };

const mapDispatchToProps = dispatch => ({
    getItems: item => {
        dispatch(getItems(item));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    )(Results);