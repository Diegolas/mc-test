import React from 'react';
import shipping from "../../../assets/ic_shipping.png";
import Currency from '../../../components/currency/Currency';

import './ResultItem.sass';


import { Link } from "react-router-dom";


const ResultItem = ({id, title, price, picture, condition, free_shipping}) => <div className="mc-result-item">
    <div className="mc-result-item__detail">
        <div className="mc-result-item__detail-img">
            <Link to={`/items/${id}`}><img src={picture} alt={title}/></Link>
        </div>
        <div className="mc-result-item__detail-info">
            <div className="mc-result-item__detail-info-amount">
                <div> <Currency {...price}/></div>
                {free_shipping && <img src={shipping}/>}
            </div>
            <div className="mc-result-item__detail-info-desc">
                <Link to={`/items/${id}`}>{title}</Link>
            </div>
        </div>
    </div>
    <div className="mc-result-item__from">{condition}</div>
</div>

export default ResultItem;