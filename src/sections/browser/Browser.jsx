import React, {Component} from 'react';
import IconInput from '../../components/icon-input/IconInput';
import './Browser.sass';
import logo from "../../assets/Logo_ML.png";
import { withRouter } from 'react-router-dom';
import { serialize } from '../../commons/utls';

class Browser extends Component {
    constructor(){
        super();
        this.state = {
            search: null,
            redirect: false
        }
    }

    handleOnSubmit = (e) => {
        const {history} = this.props;
        const {search} = this.state;
        e.preventDefault();
        if(!search) {
            return;
        }
        history.push(`/items?${serialize({search})}`);
    }

    handleOnChange = (e) => {
        const {target : {name, value}} = e;
        this.setState({[name]: value});
    }

    render() {
        
        return <div className="mc-browser">
            <div className="mc-browser__container wrapper">
                <div className="mc-browser__logo">
                    <img src={logo} alt=""/>
                </div>
                <form onSubmit={this.handleOnSubmit} className="mc-browser__search-bar">
                    <IconInput
                        onChange={this.handleOnChange}
                        name="search"
                        icon="magnify" 
                        placeholder="Nunca dejes de buscar"
                    />
                </form>

            </div>
        </div>
    }
}

export default withRouter(Browser);