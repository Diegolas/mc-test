import React, {Component} from 'react';
import { connect } from 'react-redux';
import { getItem } from '../../store/actions';

import Breadcrumb from '../../components/breadcrumb/Breadcrumb';
import ItemInfo from './components/ItemInfo';
import ItemDesc from './components/ItemDesc';
import './Item.sass';

class Item extends Component {

    componentDidMount() {
        const {match: {params}} = this.props;
        params.id && this.props.getItem(params.id);
    }
    
    render() {
        const {item} = this.props;
        return item.id ? <div className="wrapper">
            <div className="mc-item">
                <Breadcrumb categoriesList={item.categories}/>
                <div className="mc-item__container">
                    <div className="mc-item__container-img-desc">
                        <div className="mc-item__image">
                            <img src={item.picture}/>
                        </div>
                        <div className="mc-item__desc">
                            <ItemDesc {...item}/>
                        </div>
                    </div>
                    <div className="mc-item__container-info">
                        <ItemInfo {...item}/>
                    </div>
                </div>
            </div>
        </div> : ''
    }
}

const mapStateToProps = state => {
    return {
        item: state.items
    };
  };

const mapDispatchToProps = dispatch => ({
    getItem: item => {
        dispatch(getItem(item));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    )(Item);