import React from 'react';
import './ItemInfo.sass';
import Currency from '../../../components/currency/Currency';

const ItemInfo = ({condition, free_shipping, price, sold_quantity, title}) => <div className="mc-item-info">
    <div className="mc-item-info__condition-sold">{condition} - {sold_quantity} vendidos</div>
    <div className="mc-item-info__title">{title}</div>
    <div className="mc-item-info__price"><Currency {...price}/></div>
    <div className="mc-item-info__button">
        <button className="mc-btn">Comprar</button>
    </div>
</div>  
export default ItemInfo;