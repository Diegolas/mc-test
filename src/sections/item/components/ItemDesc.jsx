import React from 'react';
import './ItemDesc.sass';

const ItemDesc = ({description}) => <div className="mc-item-desc">
    <div className="mc-item-desc__title">Descripción del producto</div>
    <div className="mc-item-desc__description">{description}</div>
</div>

export default ItemDesc;