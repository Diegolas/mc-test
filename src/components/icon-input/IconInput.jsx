import React from 'react';
import magnify from '../../assets/ic_Search.png';
import './IconInput.sass';
const icons = {
    magnify
}

const IconInput = ({onChange, name, value, icon, placeholder}) => {
    const srcIcon = icons[icon] || '';
    return <div className="mc-icon-input">
        <div className="mc-icon-input__input">
            <input value={value} name={name} placeholder={placeholder} onChange={onChange} type="text"/>
        </div>
        <div className="mc-icon-input__icon">
            <button type="submit">
                <img src={srcIcon} alt=""/>
            </button>
        </div>
    </div>
}

export default IconInput;