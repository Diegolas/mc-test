import React from 'react';
import { formatCLP } from '../../commons/utls';

const format = {
    'CLP' : formatCLP
}

const Currency = ({amount, currency}) => {
    return format[currency] ? format[currency](amount) : amount;
}

export default Currency;