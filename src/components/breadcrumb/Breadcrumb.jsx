import React from 'react';
import './Breadcrumb.sass';

const Breadcrumb = ({categoriesList}) => <div className="mc-breadcrumb">
    {categoriesList.map((value, index) => <div key={index} className="mc-breadcrumb__item">
        {value}
    </div>)}
</div>

export default Breadcrumb;