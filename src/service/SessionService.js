const SESSION_VAR = 'session';
const defaultSession = {
    author: {name: "", lastname: ""}
};

const getSession = () => {
    const localSession = localStorage.getItem(SESSION_VAR);
    return localSession ? JSON.parse(localSession) : defaultSession;
};

const setSession = (session) => {
    const localSession = getSession();
    localStorage.setItem(SESSION_VAR, JSON.stringify({...localSession, ...session}));
}

export {
    getSession,
    setSession
}