import React from 'react';
import Browser from './sections/browser/Browser';

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import rootReducer from './store/reducers';
import { getItem } from './store/actions';

import './styles/index.sass';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Index from './sections/index/Index';
import Results from './sections/results/Results';
import Item from './sections/item/Item';

const store = createStore(rootReducer, applyMiddleware(thunk));

//store.dispatch(getItem(123123));

function App() {
  return (
    <Provider store={store}>
      <div className="mc-app">
          <Router>
            <Browser/>
            <div className="mc-app__path">
              <Route path="/" exact component={Index} />
              <Route path="/items" exact component={Results} />
              <Route path="/items/:id" component={Item} />
            </div>
          </Router>
      </div>
    </Provider>
  );
}

export default App;
