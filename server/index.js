import express from 'express';
import item from './db/item';
import items from './db/items';

const app = express();

app.get('/api/items', (req, res) => {
  res.status(200).send(items)
});
app.get('/api/items/:id', (req, res) => {
  res.status(200).send(item)
});

const PORT = 5000;
app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});